package com.kolchak.controller;

import com.kolchak.model.bouquet.Bouquet;
import com.kolchak.model.bouquet.bouquetType.BouquetFromCatalog;
import com.kolchak.model.bouquet.bouquetType.BouquetPersonalOrder;
import com.kolchak.model.delivery.Delivery;
import com.kolchak.model.delivery.deliveryType.ExpressDelivery;
import com.kolchak.model.delivery.deliveryType.RegularDelivery;
import com.kolchak.model.event.Event;
import com.kolchak.model.event.eventType.Birthday;
import com.kolchak.model.event.eventType.Funeral;
import com.kolchak.model.event.eventType.Wedding;
import com.kolchak.model.order.Order;
import com.kolchak.model.order.OrderImpl;
import com.kolchak.model.wrapping.Wrapping;
import com.kolchak.model.wrapping.wrappingType.CheapWrapping;
import com.kolchak.model.wrapping.wrappingType.ExpensiveWrapping;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ControllerImpl implements Controller {
    private List<Order> orderList = new LinkedList<>();
    private Scanner scanner = new Scanner(System.in);

    public void addOrder() {
        orderList.add(new OrderImpl(addBouquetType(), addDeliveryType(), addEventType(), addWrappingType()));
    }

    public void showOrderList() {
        for (int i = 0; i < orderList.size(); i++) {
            System.out.println("--------------------------");
            int orderNum = i + 1;
            System.out.println("Order number: " + orderNum);
            orderList.get(i).showOrder();

        }
    }

    private Bouquet addBouquetType() {
        System.out.println("Choose bouquet type: ");
        System.out.println("1 - from catalog.");
        System.out.println("2 - personal order.");
        int input = scanner.nextInt();
        if (input == 1) {
            return new BouquetFromCatalog();
        } else if (input == 2) {
            return new BouquetPersonalOrder();
        } else {
            System.out.println("Choose 1 or 2.");
            return addBouquetType();
        }
    }

    private Delivery addDeliveryType() {
        System.out.println("Choose delivery type: ");
        System.out.println("1 - express delivery.");
        System.out.println("2 - regular delivery.");
        int input = scanner.nextInt();
        if (input == 1) {
            return new ExpressDelivery();
        } else if (input == 2) {
            return new RegularDelivery();
        } else {
            System.out.println("Choose 1 or 2.");
            return addDeliveryType();
        }
    }

    private Event addEventType() {
        System.out.println("Choose event type: ");
        System.out.println("1 - wedding.");
        System.out.println("2 - birthday.");
        System.out.println("3 - funeral.");
        int input = scanner.nextInt();
        if (input == 1) {
            return new Wedding();
        } else if (input == 2) {
            return new Birthday();
        } else if (input == 3) {
            return new Funeral();
        } else {
            System.out.println("Choose 1 or 2 or 3.");
            return addEventType();
        }
    }

    private Wrapping addWrappingType() {
        System.out.println("Choose wrapping type: ");
        System.out.println("1 - expensive wrap.");
        System.out.println("2 - cheap wrap.");
        int input = scanner.nextInt();
        if (input == 1) {
            return new ExpensiveWrapping();
        } else if (input == 2) {
            return new CheapWrapping();
        } else {
            System.out.println("Choose 1 or 2.");
            return addWrappingType();
        }
    }

}
