package com.kolchak.view;

@FunctionalInterface
public interface Printable {
    void print();

}
