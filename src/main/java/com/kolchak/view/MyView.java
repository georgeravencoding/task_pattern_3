package com.kolchak.view;

import com.kolchak.controller.Controller;
import com.kolchak.controller.ControllerImpl;
import org.apache.logging.log4j.core.util.JsonUtils;

import java.util.*;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private Controller controller;

    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
//        menu.put("3", bundle.getString("3"));
//        menu.put("4", bundle.getString("4"));
//        menu.put("5", bundle.getString("5"));
//        menu.put("6", bundle.getString("6"));
//        menu.put("7", bundle.getString("7"));
//        menu.put("8", bundle.getString("8"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView() {
        methodsMenu = new LinkedHashMap<>();
        input = new Scanner(System.in);
        controller = new ControllerImpl();

        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodsMenu.put("1", controller::addOrder);
        methodsMenu.put("2", controller::showOrderList);
//        methodsMenu.put("3", controller::invokeThreeMethods);
//        methodsMenu.put("4", controller::setValueIntoField);
//        methodsMenu.put("5", this::InternationalizeMenuEnglish);
//        methodsMenu.put("6", this::InternationalizeMenuSpanish);
//        methodsMenu.put("7", this::InternationalizeMenuJapanese);
//        methodsMenu.put("8", this::InternationalizeMenuUkrainian);
        methodsMenu.put("Q", () -> System.exit(0));
    }


    private void InternationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void InternationalizeMenuSpanish() {
        locale = new Locale("es");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void InternationalizeMenuJapanese() {
        locale = new Locale("ja");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void InternationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }


    private void inputMenu() {
        System.out.println("MENU");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            inputMenu();
            System.out.println("Please select menu point: ");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {

            }
        } while (!keyMenu.equalsIgnoreCase("Q"));
    }

}
