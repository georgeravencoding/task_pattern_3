package com.kolchak.model.delivery.deliveryType;

import com.kolchak.model.bouquet.Bouquet;
import com.kolchak.model.delivery.Delivery;

public class ExpressDelivery implements Delivery {
    public ExpressDelivery() {
    }

    @Override
    public void showDeliveryType() {

        System.out.println("Order with express delivery.");
    }

}
