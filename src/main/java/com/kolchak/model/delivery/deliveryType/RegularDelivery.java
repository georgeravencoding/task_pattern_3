package com.kolchak.model.delivery.deliveryType;

import com.kolchak.model.delivery.Delivery;

public class RegularDelivery implements Delivery {
    public RegularDelivery() {
    }

    @Override
    public void showDeliveryType() {

        System.out.println("Order with regular delivery.");
    }
}
