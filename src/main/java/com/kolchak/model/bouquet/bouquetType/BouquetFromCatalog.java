package com.kolchak.model.bouquet.bouquetType;

import com.kolchak.model.bouquet.Bouquet;
import com.kolchak.model.delivery.Delivery;
import com.kolchak.model.event.Event;
import com.kolchak.model.wrapping.Wrapping;

public class BouquetFromCatalog implements Bouquet {

    public BouquetFromCatalog() {
    }

    @Override
    public void showBouquetType() {
        System.out.println("Bouquet from catalog.");
    }
}
