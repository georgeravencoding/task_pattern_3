package com.kolchak.model.bouquet.bouquetType;

import com.kolchak.model.bouquet.Bouquet;
import com.kolchak.model.delivery.Delivery;
import com.kolchak.model.event.Event;
import com.kolchak.model.wrapping.Wrapping;

public class BouquetPersonalOrder implements Bouquet {

    public BouquetPersonalOrder() {
    }

    @Override
    public void showBouquetType() {
        System.out.println("Bouquet is for personal order.");
    }
}
