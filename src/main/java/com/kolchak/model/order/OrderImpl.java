package com.kolchak.model.order;

import com.kolchak.model.bouquet.Bouquet;
import com.kolchak.model.delivery.Delivery;
import com.kolchak.model.event.Event;
import com.kolchak.model.wrapping.Wrapping;

public class OrderImpl implements Order {
    private Bouquet bouquet;
    private Delivery delivery;
    private Event event;
    private Wrapping wrapping;

    public OrderImpl(Bouquet bouquet, Delivery delivery, Event event, Wrapping wrapping) {
        this.bouquet = bouquet;
        this.delivery = delivery;
        this.event = event;
        this.wrapping = wrapping;
    }

    public void showOrder() {
        bouquet.showBouquetType();
        delivery.showDeliveryType();
        event.showPurposeType();
        wrapping.showWrappingType();
    }

    public Bouquet getBouquet() {
        return bouquet;
    }

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Wrapping getWrapping() {
        return wrapping;
    }

    public void setWrapping(Wrapping wrapping) {
        this.wrapping = wrapping;
    }

}
