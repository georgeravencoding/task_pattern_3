package com.kolchak.model.event.eventType;

import com.kolchak.model.event.Event;

public class Birthday implements Event {
    public Birthday() {
    }

    @Override
    public void showPurposeType() {
        System.out.println("For birthday");
    }
}
