package com.kolchak.model.event.eventType;

import com.kolchak.model.event.Event;

public class Funeral implements Event {
    public Funeral() {
    }

    @Override
    public void showPurposeType() {
        System.out.println("For funeral.");
    }
}
