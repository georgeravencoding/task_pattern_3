package com.kolchak.model.event.eventType;

import com.kolchak.model.event.Event;

public class Wedding implements Event {
    public Wedding() {
    }

    @Override
    public void showPurposeType() {
        System.out.println("For wedding.");
    }
}
