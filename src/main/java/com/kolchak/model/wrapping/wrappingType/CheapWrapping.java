package com.kolchak.model.wrapping.wrappingType;

import com.kolchak.model.wrapping.Wrapping;

public class CheapWrapping implements Wrapping {
    public CheapWrapping() {
    }

    @Override
    public void showWrappingType() {
        System.out.println("Bouquet in cheap wrapping.");
    }
}
