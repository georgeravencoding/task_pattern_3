package com.kolchak.model.wrapping.wrappingType;

import com.kolchak.model.wrapping.Wrapping;

public class ExpensiveWrapping implements Wrapping {
    public ExpensiveWrapping() {
    }

    @Override
    public void showWrappingType() {
        System.out.println("Bouquet in expensive wrapping.");
    }
}
